(function() {
	new WOW().init();
  $("body").queryLoader2({
    backgroundColor: '#3bb56c',
    barHeight: 10,
    percentage: true,
    completeAnimation: 'grow'
  });

	/**
	 * Parallax effects
	 
	$('#scene-a').parallax({
		calibrateX: true,
  		calibrateY: false,
  		invertX: true,
  		invertY: true,
  		limitX: 2,
  		limitY: 0,
  		scalarX: 4,
  		scalarY: 0,
  		frictionX: 0.2,
  		frictionY: 0
	});	

	$('#scene-b').parallax({
		calibrateX: true,
  		calibrateY: false,
  		invertX: false,
  		invertY: false,
  		limitX: 2,
  		limitY: 0,
  		scalarX: 2,
  		scalarY: 0,
  		frictionX: 0.2,
  		frictionY: 0
	});
  */
 
 $('.next-about').on('click touchstart',function(e) {
  e.preventDefault();
  $('.orbit-next').trigger('click');
 });

 $('.prev-about').on('click touchstart',function(e) {
  e.preventDefault();
  $('.orbit-prev').trigger('click');
 });


}());


var DF_APP;

if(typeof DF_APP === 'undefined') {
  DF_APP = {};
}

DF_APP = {

  activeElements : function(parent,element) {
    $(element,parent).on('click touchstart',function(e) {
      e.preventDefault();
      $(this).addClass('active').siblings(element).removeClass('active');
    });
  },

  sliderRequest : function(parent,element) {
    $.each($(element,parent), function() {
      $(this).on('click touchstart',function() {
        var dt = $(this).data('slider');

        $.get('sliders/'+ dt +'.html', function(data) {
          
          $('.slider-figure').css('opacity',0);
          function getClicked() {
            
            $('.slider-figure').html(data);
            new WOW().init();
            $('.slider-figure').css('opacity',1);

          }
          setTimeout(getClicked,500);
        });
      });
    });
  },

  /**
   * Animations in product page
   * @param  {String} element Parent
   * @param  {String} bullet  Animation element
   * @param  {String} value   100% right / -100% left
   * @return {HTML}           Attribute style
   */
  animationInfo: function(element,bullet,value) {
    $(element).hover(
      
      function() {
        $(bullet).css({ transform : 'translateX('+ value +')' });
        function returnUp() {
          $(bullet).css({ transform : 'translateX(0)', zIndex: 999 });
        };  
        setTimeout(returnUp, 250);
      },
      
      function() {
        $(bullet).css({ transform : 'translateX('+ value +')' });
        function returnDown() {
          $(bullet).css({ transform : 'translateX(0)' });
        };
        setTimeout(returnDown, 250);
        $(bullet).css({ zIndex: 0 });
      }

    );
  }

};

DF_APP.activeElements('.bullets','li');
DF_APP.sliderRequest('.bullets','li');

//Products animations
DF_APP.animationInfo('.product-figure-a','.green','100%');
DF_APP.animationInfo('.product-figure-b','.violet','-100%');
DF_APP.animationInfo('.product-figure-c','.orange','100%');

function moveBg() {
  $window = $(window);               
  $('*[data-type="background"]').each(function(){
    var $bgobj = $(this); 
    $(window).scroll(function() {                      
      var yPos = -($window.scrollTop() / $bgobj.data('speed') / 1); 
      var coords = 'center '+ yPos + 'px';
      $bgobj.css({ backgroundPosition: coords });
    }); 
  });
}
moveBg();

function initSlider() {
  $(window).on('load',function() {
    var $el = $('figure.active','.nav-about-content');
    var next_title = $el.next().data('slidetitle'),
        prev_title = $('figure:last','.nav-about-content').data('slidetitle');
    $('.get-prev-title').text(prev_title);
    $('.get-next-title').text(next_title);
  });

  $('.nav-slide-arrow').hover(
    function() {
      $('.nav-slide-title',this).stop().show('last');
    },
    function() {
      $('.nav-slide-title',this).stop().hide('last');
    }
  );  
};

initSlider();

function getActiveSlider() {
    var dt, nmsg, pmsg;
    
    $.each($('figure','.nav-about-content'),function() {
        if($(this).hasClass('active')) {
            dt = $(this).data('slidenumber');

            if($(this).next().length) {
              nmsg = $(this).next().data('slidetitle');
            } else {
              nmsg = $('figure:first','.nav-about-content').data('slidetitle');
            }
            $('.get-next-title').text(nmsg);

            if($(this).prev().length) {
              pmsg = $(this).prev().data('slidetitle');
            } else {
              pmsg = $('figure:last','.nav-about-content').data('slidetitle');
            }
            $('.get-prev-title').text(pmsg);
        }
    });

    $.each($('div','.list-abouts'),function() {
        if($(this).data('slidenumber') === dt) {
            $(this).addClass('active').siblings('div').removeClass('active');
        }
    });
    new WOW().init();
};

function aboutSlider() {
    $('.prev-about').on('click touchstart',getActiveSlider);
    $('.next-about').on('click touchstart',getActiveSlider);
};

//aboutSlider();
//

function revealFruit() {
  $('.figure-thumb').on('click touchstart',function() {
    var fruit = $('.fruit-name',this).find('span').text(),
        color = $('.fruit-name',this).find('span').css('backgroundColor');
    $('.fruit-title').text(fruit);

    $('.reveal-modal').css('borderColor',color)
    .find('.red').css('color',color)
    .end()
    .find('.close-reveal-modal').css('backgroundColor',color);
  });
};

revealFruit();

function getCirclesColors(el,tg,color) {
  $(el).hover(
    function() {
      $(tg).css('color',color);
    },
    function() {
      $(tg).css('color','white');
    }
  );
};
getCirclesColors('.orange-circle','.need-this a','#6c5589');
getCirclesColors('.violet-circle','.buy-this a','#f9bf45');

$('.get-form').on('click touchstart',function() {
  var dt = $(this).data('formname');
  console.log(dt);
});

// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation({
    orbit : {
        after_slide_change: getActiveSlider
    }
});


